// Update to the previous versions of ECMAScript
// ES6 = is an update to the previous versions of ECMA Script
// ES6 Update brings new features to JavaScript.

/* 
   Focus on Basics to write less and do more.
*/
// Code discussion
// console.log("Hello World!");

// ECMAScript is the standard that is used to create implementations of the language, one which is JavaScript.
// Where all browser vendor can implement (Apple, Google, Microsoft, Mozilla, etc.)

// New features to JavaScript

// SECTION Exponent Operator

// Using Math object methods.
const firstNum = Math.pow(8, 2);
console.log(firstNum);

// Using the exponent operator
const secondNum = 8 ** 2;
console.log(secondNum);

//SECTION Template Literals

let name = "John";

let message = "Hello " + name + "! Welcome to programming!";
console.log("Message without template literals.");
console.log(message);
// SECTION Template literals
// Allows to write sting without suing the concatenation operator (+)
// ${} is called
// Uses backticks instead of ("") ('')

message = `Hello ${name}! Welcome to programming!`;
console.log("Message with template literals.");
console.log(message);

const anotherMessage = `${name} attended a math competition.
 He won it by solving the problem 8 ** 2 with the solution of ${8 ** 2}`;
console.log(anotherMessage);

//[SECTION] Array Desctructuring
// It allows us to name array elements with variableName of using the instead of using the index numbers.
// - Syntax
/* 
      let/const [variableName1,variableName2, variableName3] = arrayName;
   */

const fullName = ["Juan", "Dela", "Cruz"];

// Pre-Array Destructuring
console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);
// Hello Juan Dela Cruz! It's nice to meet you.

console.log(
  `Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}! It's nice to meet you.`
);
// Array Destructuring
// variable naming for array destructuring is based on the developers choice.
const [firstName, middleName, lastName] = fullName;

console.log(
  `Hello ${firstName} ${middleName} ${lastName}! It's nice to meet you.`
);

// [SECTION] Object Destructuring
/* 
      - Shortens the syntax for accessing properties form object
      - The difference with array destructuring, this should be exact property name.

      -Syntax:
         let/const {propertyNameA, propertyNameB, propertyC, ..etc.} = object;
   */
// Create an Object
const person = {
  givenName: "Jane",
  maidenName: "Dela",
  surName: "Cruz",
};
// console.log(person.givenName);
// console.log(person.maidenName);
// console.log(person.surName);

// console.log(
//   `Hello ${person.givenName} ${person.maidenName} ${person.surName}! It's good to see you again`
// );

// const { givenName, maidenName, surName } = person;
// console.log(givenName);
// console.log(maidenName);
// console.log(lastName);

function getFullName({ givenName, maidenName, surName }) {
  console.log(`Hello ${givenName} ${maidenName} ${surName}`);
}

getFullName(person);

//[SECTION] Arrow Function
/* 
   - Compact alternative syntax to traditional functions.
   - Useful for code snippets where creating functions will not be reusable in any other portion of code.
   - "DRY (Don't Repeat Yourself)" principle
   - This will only work with function expression.
   - Syntax:
      let/const variableName = () =>{
         //code block/statement
      }
      let/const variableName = (parameter) =>{
         //code block/statement with parameter
      } 

*/
// hello();
// //function declaration

// function hello() {
//   console.log(`Hello World!`);
// }
// hello();

let hello = () => {
  console.log(`Hello World`);
};
hello();

const students = ["John", "Jane", "Judy"];

// Arrow Functions using Array Iteration Method
// Pre Arrow Function

// students.forEach(function (student) {
//   console.log(`${student} is a student.`);
// });

// Arrow Function
students.forEach((student) => {
  console.log(`${student} is a student`);
});

const add = (x, y) => x + y;
let total = add(1, 2);

console.log(total);

const numbers = [1, 2, 3, 4, 5, 6];
let squareValues = numbers.map((number) => number ** 2);
console.log(squareValues);

//SECTION Default Function Argument Value
// provides a default argument value if none is provided when the function is invoked.
const greet = (name = "User") => `Good morning, ${name}`;

console.log(greet()); //no argument provided will result to undefined.
console.log(greet("Sweet"));

//[SECTION] Object constructor Class-based Object Blueprint
/* 
   - Syntax:
      class className{
         constructor(objectPropertyA, objectPropertyB)
      }

*/

class Car {
  constructor(brand, name, year) {
    this.brand = brand;
    this.name = name;
    this.year = year;
  }
}
/* function constructor
   function Car(brand,name year){
      this.brand = brand;
      this.name = name;
      this.year = year;
   }
*/
// "new" operator creates/instantiates a new object with the given  argument as value of it's property.
const myCar = new Car();
console.log(myCar); //we have create a object but it's property is undefined.
//
myCar.brand = "Ford";
myCar.name = "Ranger Raptor";
myCar.year = 2021;
console.log(myCar);
